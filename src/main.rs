#![feature(duration_as_u128)]

extern crate clap;

use clap::{App, Arg};
use std::fmt;
use std::fs;
use std::io;
use std::io::Read;
use std::io::Seek;
use std::io::SeekFrom;
use std::io::Write;
use std::process::exit;
use std::time::SystemTime;

const NAME: &'static str = env!("CARGO_PKG_NAME");
const DESCRIPTION: &'static str = env!("CARGO_PKG_DESCRIPTION");
const VERSION: &'static str = env!("CARGO_PKG_VERSION");
const AUTHORS: &'static str = env!("CARGO_PKG_AUTHORS");
const MAX_SIZE: usize = 16384;
const UPDATE_FREQ: u128 = 1000;
const BYTE_UNITS: &'static [&'static str] = &["kB", "MB", "GB", "TB", "PB", "EB", "ZB", "YB"];
const TIME_UNITS: &'static [&'static str] = &["m", "h"];

struct Sector {
    data: [u8; MAX_SIZE],
    size: usize,
    sector_size: usize,
}

impl fmt::Debug for Sector {
    fn fmt(&self, formatter: &mut fmt::Formatter) -> fmt::Result {
        self.data[..self.size].fmt(formatter)
    }
}

impl Sector {
    fn new(sector_size: usize) -> Sector {
        Sector {
            data: [0; MAX_SIZE],
            size: 0,
            sector_size,
        }
    }

    fn is_empty(&self) -> bool {
        for i in 0..self.size {
            if self.data[i] != 0 {
                return false;
            }
        }

        return true;
    }

    fn read_from<R: ?Sized>(&mut self, reader: &mut R) -> io::Result<bool>
    where
        R: Read + Seek,
    {
        match reader.read(&mut self.data[..self.sector_size]) {
            Ok(size) => self.size = size,
            Err(err) => {
                eprintln!("{}", err);
                reader.seek(SeekFrom::Current(self.sector_size as i64))?;
                self.size = self.sector_size;
                self.data = [0; MAX_SIZE];
            }
        }

        Ok(self.size > 0)
    }

    fn write_to<W: ?Sized>(&self, writer: &mut W) -> io::Result<usize>
    where
        W: Write + Seek,
    {
        if self.is_empty() {
            writer.seek(SeekFrom::Current(self.size as i64))?;

            Ok(0)
        } else {
            Ok(writer.write(&self.data[..self.size])?)
        }
    }
}

macro_rules! format_bytes {
    ($value:expr) => {
        BYTE_UNITS
            .iter()
            .enumerate()
            .map(|(i, u)| ($value / 1000_u64.pow(i as u32 + 1), u))
            .take_while(|(i, _)| *i > 1000)
            .map(|(i, u)| format!("{} {}", i, u))
            .last()
            .unwrap_or(format!("{} B", $value))
    };
}

macro_rules! format_time {
    ($value:expr) => {
        TIME_UNITS
            .iter()
            .enumerate()
            .map(|(i, u)| ($value / 60_u64.pow(i as u32 + 1), u))
            .take_while(|(i, _)| *i > 10)
            .map(|(i, u)| format!("{}{}", i, u))
            .last()
            .unwrap_or(format!("{}s", $value))
    };
}

macro_rules! log_progress {
    ($total:expr, $copied:expr, $start_time:expr, $count:expr) => {
        #[allow(unused_must_use)]
        {
            print!("\x1b[1A\x1b[1A\x1b[1A\x1b[1A");
            println!(
                "progress: {} ({}) of {} ({}) copied ({:.2}%)\x1b[K",
                $total,
                format_bytes!($total),
                $count,
                format_bytes!($count),
                $total as f64 / $count as f64 * 100.0
            );
            println!(
                "skipped: {} ({:.2}%)\x1b[K",
                format_bytes!($total - $copied),
                ($copied as f64 / $total as f64 * 100.0)
            );
            println!(
                "run time: {} of {} remaining\x1b[K",
                format_time!($start_time.elapsed().map(|x| x.as_secs()).unwrap_or(0)),
                format_time!(
                    ($start_time.elapsed().map(|x| x.as_secs()).unwrap_or(0) as f64
                        / ($total as f64 / $count as f64)) as u64
                )
            );
            println!(
                "speed average: {}/s\x1b[K",
                format_bytes!(
                    $total
                        .checked_div($start_time.elapsed().map(|x| x.as_secs()).unwrap_or(0))
                        .unwrap_or(0)
                )
            );
            io::stdout().flush().unwrap();
        }
    };
}

fn ddrescue<R: ?Sized, W: ?Sized>(
    input: &mut R,
    output: &mut W,
    sector_size: usize,
) -> io::Result<()>
where
    R: Read + Seek,
    W: Write + Seek,
{
    let mut total: u64 = 0;
    let mut copied: u64 = 0;
    let start_time = SystemTime::now();
    let mut last_update = SystemTime::now();
    let mut buffer = Sector::new(sector_size);
    let count = input.seek(SeekFrom::End(0))?;
    input.seek(SeekFrom::Start(0))?;
    print!("...\n\n\n\n");

    loop {
        if !buffer.read_from(input)? {
            break;
        }

        total += buffer.size as u64;
        copied += buffer.write_to(output)? as u64;

        if last_update.elapsed().map(|x| x.as_millis()).unwrap_or(0) >= UPDATE_FREQ {
            log_progress!(total, copied, start_time, count);
            last_update = SystemTime::now();
        }
    }
    log_progress!(total, copied, start_time, count);

    Ok(())
}

fn run(source: &str, target: &str, sector_size: usize) -> io::Result<()> {
    let mut input = fs::File::open(source)?;
    let mut output = fs::OpenOptions::new()
        .create(true)
        .write(true)
        .open(target)?;

    ddrescue(&mut input, &mut output, sector_size)
}

fn main() {
    let matches = App::new(NAME)
        .version(VERSION)
        .author(AUTHORS)
        .about(DESCRIPTION)
        .arg(
            Arg::with_name("sector_size")
                .short("b")
                .long("sector-size")
                .takes_value(true)
                .help("sector size of input device [default 512]"),
        )
        .arg(
            Arg::with_name("source")
                .required(true)
                .help("Source device"),
        )
        .arg(
            Arg::with_name("target")
                .required(true)
                .help("Target device"),
        )
        .get_matches();

    if let Err(err) = run(
        matches.value_of("source").unwrap(),
        matches.value_of("target").unwrap(),
        matches
            .value_of("sector_size")
            .map(|x| x.parse::<usize>().expect("could not parse numeric value"))
            .unwrap_or(512),
    ) {
        eprintln!("{}", err);
        exit(1);
    }
}

#[test]
fn is_empty() {
    let mut s = Sector::new(4);
    s.size = 4;
    assert!(s.is_empty());
}

#[test]
fn is_not_empty() {
    let mut s = Sector::new(4);
    s.size = 4;
    s.data[0] = 1;
    assert!(!s.is_empty());

    let mut s = Sector::new(4);
    s.size = 4;
    s.data[3] = 1;
    assert!(!s.is_empty());

    let mut s = Sector::new(4);
    s.size = 4;
    s.data[4] = 1;
    assert!(s.is_empty());
}

#[test]
fn skip_zero_sectors() {
    use std::io::Cursor;

    let mut reader = Cursor::new(vec![0; 4]);
    let mut buff = Cursor::new(vec![1, 2, 3, 4]);
    ddrescue(&mut reader, &mut buff, 4).unwrap();
    assert_eq!(&buff.get_ref()[..], &[1, 2, 3, 4]);

    let mut reader = Cursor::new(vec![0; 2]);
    let mut buff = Cursor::new(vec![1, 2, 3, 4]);
    ddrescue(&mut reader, &mut buff, 4).unwrap();
    assert_eq!(&buff.get_ref()[..], &[1, 2, 3, 4]);

    let mut reader = Cursor::new(vec![]);
    let mut buff = Cursor::new(vec![1, 2, 3, 4]);
    ddrescue(&mut reader, &mut buff, 4).unwrap();
    assert_eq!(&buff.get_ref()[..], &[1, 2, 3, 4]);
}

#[test]
fn copies() {
    use std::io::Cursor;

    let mut reader = Cursor::new(vec![1; 4]);
    let mut buff = Cursor::new(vec![2; 4]);
    ddrescue(&mut reader, &mut buff, 4).unwrap();
    assert_eq!(&buff.get_ref()[..], &[1, 1, 1, 1]);

    let mut reader = Cursor::new(vec![1; 2]);
    let mut buff = Cursor::new(vec![2; 4]);
    ddrescue(&mut reader, &mut buff, 4).unwrap();
    assert_eq!(&buff.get_ref()[..], &[1, 1, 2, 2]);
}

#[test]
fn skip_zeroes_and_seek() {
    use std::io::Cursor;

    let mut reader = Cursor::new(vec![0, 0, 0, 0, 1, 1, 1, 1]);
    let mut buff = Cursor::new(vec![2, 2]);
    ddrescue(&mut reader, &mut buff, 4).unwrap();
    assert_eq!(buff.get_ref().len(), 8);
    assert_eq!(&buff.get_ref()[..], &[2, 2, 0, 0, 1, 1, 1, 1]);
}

#[cfg(test)]
struct FailingRead {
    failed: bool,
    position: i64,
}

#[cfg(test)]
impl FailingRead {
    fn new() -> FailingRead {
        FailingRead {
            failed: false,
            position: 0,
        }
    }
}

#[cfg(test)]
impl Read for FailingRead {
    fn read(&mut self, _buffer: &mut [u8]) -> io::Result<usize> {
        if !self.failed {
            self.failed = true;
            return Err(io::Error::new(
                io::ErrorKind::Other,
                "I/O error: failed to read!",
            ));
        }

        Ok(0)
    }
}

#[cfg(test)]
impl Seek for FailingRead {
    fn seek(&mut self, from: io::SeekFrom) -> io::Result<u64> {
        match from {
            io::SeekFrom::Current(x) => self.position += x,
            _ => {}
        }

        Ok(self.position as u64)
    }
}

#[test]
fn skip_failing_sectors() {
    use std::io::Cursor;

    let mut reader = FailingRead::new();
    let mut buff = Cursor::new(vec![]);
    ddrescue(&mut reader, &mut buff, 4).unwrap();
    assert!(reader.failed);
    assert_eq!(reader.position, 4);

    let mut reader = Cursor::new(vec![1, 1, 1, 1]);
    ddrescue(&mut reader, &mut buff, 4).unwrap();
    assert_eq!(buff.get_ref().len(), 8);
    assert_eq!(&buff.get_ref()[..], &[0, 0, 0, 0, 1, 1, 1, 1]);
}
