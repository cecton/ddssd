ddssd
=====

A tool that clone the data of a device to another (ddrescue), skipping the
sectors filled with zeroes (use it for SDD).

Installation
------------

*Requires Rust nightly.*

```
git clone https://gitlab.com/cecton/ddssd.git
cd ddssd
cargo build --release
```

Usage
-----

```
./target/release/ddssd /dev/sdX /dev/sdY
```
